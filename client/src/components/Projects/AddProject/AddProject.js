import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { getProjectsQuery } from '../graphql/queries';
import { addProjectMutation } from '../graphql/mutations';
import * as compose from 'lodash.flowright';

class AddProject extends Component {
  constructor(props){
    super(props);
    this.state = {
      title: '',
      description: '',
      url: ''
    };
  }
  submitForm(e){
    e.preventDefault();
    this.props.addProjectMutation({
      variables: {
        title: this.state.title,
        description: this.state.description,
        url: this.state.url
      },
      refetchQueries: [{query:getProjectsQuery}]
    });
  }
  render(){
    return(
        <React.Fragment>
            <h2>Add a Project</h2>
            <form onSubmit={ this.submitForm.bind(this) }>
                <div className="form-group">
                    <label>Title</label>
                    <input type="text" className="form-control" onChange={(e) => this.setState({title: e.target.value})}/>
                </div>
                <div className="form-group">
                    <label>Description</label>
                    <input type="text" className="form-control" onChange={(e) => this.setState({description: e.target.value})} />
                </div>
                <div className="form-group">
                    <label>Url</label>
                    <input type="text" className="form-control" onChange={(e) => this.setState({url: e.target.value})} />
                </div>
                <button className="btn btn-primary">+ Add Project</button>
            </form>
        </React.Fragment>
    );
  }
}

export default compose(
  graphql(addProjectMutation, {name: "addProjectMutation"})
)(AddProject);

import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { getProjectsQuery } from './graphql/queries';
import AddProject from './AddProject/AddProject';

class ProjectsList extends Component {
  constructor(props){
    super(props);
    this.state = {
      selected: null
    }
  }
  displayProjects(){
    var data = this.props.data;
    if(data.loading){
      return(<div>Loading projects...</div>);
    } else {
      return data.projects.map(project => {
        return (
          <div key={project.id}>
            <h1 onClick={ (e) => {this.setState({selected: project.id})} }>{project.title}</h1>
            <p>{project.description}</p>
            <a href={project.url} target="_blank">View Site</a>
          </div>
        )
      })
    }
  }
  render(){
    return(
      <div className="container">
        <div className="row">
          <div className="col-12">
            <h1>Projects</h1>
            {this.displayProjects()}
            <hr />
            <AddProject />
            <hr />
          </div>
        </div>
      </div>
    );
  }
}

export default graphql(getProjectsQuery)(ProjectsList);

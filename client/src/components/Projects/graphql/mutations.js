import { gql } from 'apollo-boost';

const addProjectMutation = gql`
  mutation($title: String!, $description: String!, $url: String!) {
    addProject (title: $title, description: $description, url: $url) {
      id
      title
      description
      url
    }
  }
`

export { addProjectMutation };

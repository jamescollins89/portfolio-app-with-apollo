import { gql } from 'apollo-boost';

const getProjectsQuery = gql`
  {
    projects {
      id
      title
      description
      url
    }
  }
`

export { getProjectsQuery };

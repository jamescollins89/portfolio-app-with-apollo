import React from 'react';
import { Link, NavLink } from 'react-router-dom';

const Navbar = () => (
  <div className="header pt-3 pb-3 primary-background-color">
    <div className="container">
      <div className="row">
        <div className="col">

        </div>
        <div className="col d-flex align-items-center justify-content-between">
          <NavLink to="/" activeClassName="is-active" exact>Home</NavLink>
          <NavLink to="/projects" activeClassName="is-active">Projects</NavLink>
        </div>
      </div>
    </div>
  </div>
);
export default Navbar;

import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import Navbar from './components/Navbar';
import Home from './components/Home';
import Projects from './components/Projects/Projects';

const client = new ApolloClient({
  uri: 'http://localhost:4000/graphql'
});

export default class Portfolio extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <Navbar />
          <Route exact path='/' component={Home}/>
          <ApolloProvider client={client}>
            <Route exact path='/projects' component={Projects}/>
          </ApolloProvider>
        </div>
      </BrowserRouter>
    );
  }
};
To run server use:
nodemon app

To view GraphiQL on the server
localhost:4000/graphql

Global Dependencies:
https://github.com/remy/nodemon
npm install nodemon -g